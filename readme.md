# Description
This project showcases an application of a the minimax algorithm applied
on Tic Tac Toe and Connect 4. The minimax implementation is abstractly done
on both game types, while each game module implements its custom rules.

# Requirements
Requires Python 3.7 installed.
Run the following to install requirements into a virtualenv
```
virtualenv -p python3.7 venv
source venv/bin/activate
pip install -r requirements.txt
```

To use the graph visualizer you need to install Graphviz

```sudo apt install graphviz```

# Usage

## Play an interactive game
```
python -m connect4.play
```

## Generate decision tree
```
python3 -m connect4.tree_visualizer
```
The png image should be generated inside the graph-output folder
if it didn't open automatically

## Profile
```
python3 -m connect4.profile
```
