from typing import *
from enum import Enum


class Player(Enum):
    X = "X"
    Y = "O"
    N = "."


Move = Tuple[int, int]


class Game:
    def get_winner(self) -> Optional[Player]:
        pass

    def get_possible_moves(self) -> List[Move]:
        pass

    def apply_move(self, move: Move, deep=True):
        pass

    def get_hash(self):
        pass
