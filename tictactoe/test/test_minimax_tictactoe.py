from decision_tree import minimax
import tictactoe.game
from tictactoe.game import Player

game = tictactoe.game.Game()


def test_simple():
    game.board = tictactoe.game.Board([
        [Player.X, Player.Y, None],
        [Player.X, Player.Y, None],
        [None, None, None]
    ])
    # X wins
    assert minimax(game).get_best_move() == (2, 0)
    # Y wins
    game.current_player = Player.Y
    assert minimax(game).get_best_move() == (2, 1)


def test_race():
    game.board = tictactoe.game.Board([
        [Player.X, Player.Y, None],
        [Player.Y, Player.X, None],
        [None, None, None]
    ])
    assert minimax(game).get_best_move() == (2, 2)

    game.current_player = Player.Y
    assert minimax(game).get_best_move() == (2, 2)


def test_win_in_2():
    game.board = tictactoe.game.Board([
        [Player.X, Player.Y, None],
        [Player.Y, None, None],
        [Player.X, None, Player.X]
    ])
    game.current_player = Player.Y
    game.apply_move(minimax(game).get_best_move(), False)
    game.apply_move(minimax(game).get_best_move(), False)
    assert game.get_winner() == Player.X


def test_draw_out_game():
    game.board = tictactoe.game.Board([
        [None, Player.X, None],
        [None, None, Player.X],
        [Player.Y, Player.Y, Player.X]
    ])
    game.current_player = Player.Y
    assert minimax(game).get_best_move() == (0, 2)
    while game.get_winner() is None:
        game.apply_move(minimax(game).get_best_move(), False)
    assert game.get_winner() == Player.X


def test_no_winner():
    game = tictactoe.game.Game()
    tree = minimax(game)
    while not game.get_winner() and len(game.get_possible_moves()) != 0:
        best_move = tree.get_best_move()
        if best_move is not None:
            game.apply_move(best_move, False)
            tree = next(t.next for t in tree.moves if t.move == best_move)
    assert game.get_winner() is None