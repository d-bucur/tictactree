import tictactoe.game
from tictactoe.game import Player


def test_win_conditions():
    game = tictactoe.game.Game()
    game.board = tictactoe.game.Board([
        [Player.X, Player.Y, None],
        [Player.X, Player.Y, None],
        [Player.X, None, None]
    ])
    assert game.get_winner() == Player.X

    game.board = tictactoe.game.Board([
        [Player.X, Player.Y, None],
        [Player.X, Player.Y, None],
        [None, Player.Y, None]
    ])
    assert game.get_winner() == Player.Y

    game.board = tictactoe.game.Board([
        [Player.Y, Player.X, None],
        [Player.X, Player.Y, None],
        [None, Player.Y, Player.Y]
    ])
    assert game.get_winner() == Player.Y

    game.board = tictactoe.game.Board([
        [None, Player.X, Player.Y],
        [Player.X, Player.Y, None],
        [Player.Y, Player.Y, Player.X]
    ])
    assert game.get_winner() == Player.Y

    game.board = tictactoe.game.Board([
        [None, Player.X, Player.Y],
        [Player.X, Player.Y, None],
        [Player.X, Player.Y, Player.X]
    ])
    assert game.get_winner() is None

    game.board = tictactoe.game.Board([
        [Player.X, Player.Y, None],
        [Player.X, Player.Y, None],
        [None, None, None]
    ])
    assert game.get_winner() is None


def test_moves():
    game = tictactoe.game.Game()
    game.board = tictactoe.game.Board([
        [None, None, None],
        [None, None, None],
        [None, None, None]
    ])
    start_player = game.current_player
    assert sum(1 for _ in game.get_possible_moves()) == 9
    game.apply_move((0, 0), False)
    assert sum(1 for _ in game.get_possible_moves()) == 8
    assert game.current_player != start_player
    game.apply_move((1, 0), False)
    assert sum(1 for _ in game.get_possible_moves()) == 7
    assert game.current_player == start_player


def test_copy():
    game = tictactoe.game.Game()
    game.board = tictactoe.game.Board([
        [None, None, None],
        [None, None, None],
        [None, None, None]
    ])
    start_player = game.current_player
    assert sum(1 for _ in game.get_possible_moves()) == 9
    new_game = game.apply_move((0, 0))
    assert sum(1 for _ in new_game.get_possible_moves()) == 8
    assert new_game.current_player != start_player
    new_game = new_game.apply_move((1, 0))
    assert sum(1 for _ in new_game.get_possible_moves()) == 7
    assert new_game.current_player == start_player
    assert sum(1 for _ in game.get_possible_moves()) == 9
