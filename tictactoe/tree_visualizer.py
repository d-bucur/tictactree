from decision_tree import *
import tictactoe.game
from graphviz import Digraph


def draw_graph(tree: DecisionTreeNode):
    dot = Digraph(comment='The Round Table')
    _draw_node(dot, tree)
    dot.render('graph-output/decision-tree', format='png', view=True)


def _draw_node(dot, tree: DecisionTreeNode):
    txt = "{}Score: {}\n".format(str(tree.board), tree.score)
    if tree.best_move_pos is None:
        if tree.score < 0:
            txt += "X wins"
            dot.node(str(tree.board), txt, shape='box', color='lightblue2', style='filled')
        else:
            txt += "O wins"
            dot.node(str(tree.board), txt, shape='box', color='violetred1', style='filled')
    else:
        txt += "Best: {}".format(str(tree.get_best_move()))
    dot.node(str(tree.board), txt, shape='box')
    for m in tree.moves:
        _draw_node(dot, m.next)
        dot.edge(str(tree.board), str(m.next.board), label=str(m.move))


if __name__ == "__main__":
    game = tictactoe.game.Game()
    game.board = tictactoe.game.Board([
        [Player.X, Player.Y, None],
        [Player.Y, None, None],
        [Player.X, None, Player.X]
    ])
    #game.current_player = Player.Y

    draw_graph(minimax(game, debug=True))
