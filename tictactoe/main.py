from tictactoe import game
from decision_tree import minimax

if __name__ == "__main__":
    game = game.Game()
    print(game.board)
    while not game.get_winner() and len(game.get_possible_moves()) != 0:
        # Recalculates minimax each time to debug times.
        # For an efficient tree reuse see test_minimax_tictactoe.test_no_winner()
        tree = minimax(game)
        if tree.get_best_move() is not None:
            game.apply_move(tree.get_best_move(), False)
            print(game.board)
