import copy
from dataclasses import dataclass
from typing import *

from game_interface import Player, Move
import game_interface


class Board:
    pieces: str

    def __init__(self, pieces: List[List[Optional[Player]]]) -> None:
        vals = ""
        for i in range(3):
            for j in range(3):
                p = pieces[i][j]
                vals += p.value if p is not None else Player.N.value
        self.pieces = vals

    def __getitem__(self, a: tuple):
        return self.pieces[a[0] * 3 + a[1]]

    def __setitem__(self, key: tuple, value: Player):
        k = key[0] * 3 + key[1]
        self.pieces = self.pieces[:k] + value.value + self.pieces[k+1:]

    def __str__(self):
        s = ""
        for i in range(3):
            for j in range(3):
                s += self[i, j]
            s += '\n'
        return s

    def make_move(self, x: int, y: int, player: Player):
        self[x, y] = player

    def get_winner(self) -> Optional[Player]:
        winner = self._get_winner()
        if winner != Player.N.value:
            return Player(winner)
        else:
            return None

    def _get_winner(self) -> Optional[str]:
        for i in range(3):
            if self[i, 0] == self[i, 1] == self[i, 2]:
                return self[i, 0]
            if self[0, i] == self[1, i] == self[2, i]:
                return self[0, i]
        if self[0, 0] == self[1, 1] == self[2, 2]:
            return self[0, 0]
        if self[0, 2] == self[1, 1] == self[2, 0]:
            return self[0, 2]
        return Player.N.value


@dataclass
class Game(game_interface.Game):
    current_player: Player = Player.X
    board: Board = Board([[None for x in range(3)] for y in range(3)])

    def get_winner(self) -> Optional[Player]:
        return self.board.get_winner()

    def get_possible_moves(self) -> List[Move]:
        moves = list()
        for x in range(3):
            for y in range(3):
                if self.board[x, y] == Player.N.value:
                    moves.append((x, y))
        return moves

    def apply_move(self, move: Move, deep=True):
        if deep:
            used_game = copy.deepcopy(self)
        else:
            used_game = self

        used_game.board[move] = self.current_player
        used_game.current_player = Player.Y if self.current_player == Player.X else Player.X
        return used_game

    def get_hash(self):
        return self.current_player.value + self.board.pieces
