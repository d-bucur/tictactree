from colorama import Fore, Style
from connect4 import game
from decision_tree import minimax
from game_interface import Player

DEPTH = 8


def print_board():
    print('\n\n')
    board_str = "\n\n".join(["{}  {}".format(i, "  ".join(x)) for i, x in enumerate(str(game.board).strip().split('\n'))])
    for c in board_str:
        if c == Player.X.value:
            print(Fore.RED + c + Style.RESET_ALL, end='')
        elif c == Player.Y.value:
            print(Fore.YELLOW + c + Style.RESET_ALL, end='')
        else:
            print(c, end='')
    print('\n')
    print("  ".join(" 0123456"))


if __name__ == "__main__":
    game = game.Game()

    while True:
        print_board()
        winner = game.get_winner()
        if winner is not None:
            print("Player {} wins. MUCH VICTORY!!1".format(winner.value))
            break

        if game.current_player == Player.X:
            input_pos = int(input())
            try:
                move = next(m for m in game.get_possible_moves() if m[1] == input_pos)
            except StopIteration:
                print("Invalid move...")
                continue
            game.apply_move(move, False)
        else:
            print("Computer is thinking...")
            best_move = minimax(game, DEPTH).get_best_move()
            game.apply_move(best_move, False)
            print("Move to {}".format(best_move[1]))
