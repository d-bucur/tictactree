import copy
from dataclasses import dataclass
from typing import *

from game_interface import Player, Move
import game_interface

_XMAX = 6
_YMAX = 7


@dataclass
class Board:
    pieces: str  # TODO optimize structure?

    @staticmethod
    def build_from_str(board: str):
        return Board(board.replace('\n', '').replace(' ', ''))

    def __getitem__(self, a: tuple):
        key = a[0] * _YMAX + a[1]
        return self.pieces[key]

    def __setitem__(self, key: tuple, value: Player):
        k = key[0] * _YMAX + key[1]
        self.pieces = self.pieces[:k] + value.value + self.pieces[k+1:]

    def __str__(self):
        s = ""
        for i in range(_XMAX):
            for j in range(_YMAX):
                s += self[i, j]
            s += '\n'
        return s

    def make_move(self, x: int, y: int, player: Player):
        self[x, y] = player

    def _get_winner(self) -> Optional[str]:
        # TODO extract to functions and profile time
        # horizontal
        for i in range(_XMAX):
            last_count = 0
            last = self[i, 0]
            for j in range(_YMAX):
                v = self[i, j]
                if v == last:
                    last_count += 1
                    if last_count >= 4 and last != Player.N.value:
                        return last
                else:
                    last = v
                    last_count = 1

        # vertical
        for j in range(_YMAX):
            last_count = 0
            last = self[0, j]
            for i in range(_XMAX):
                v = self[i, j]
                if v == last:
                    last_count += 1
                    if last_count >= 4 and last != Player.N.value:
                        return last
                else:
                    last = v
                    last_count = 1

        # / diagonal
        for i in range(3, 6):
            for j in range(4):
                if self[i, j] != Player.N.value and \
                        self[i, j] == self[i-1, j+1] == self[i-2, j+2] == self[i-3, j+3]:
                    return self[i, j]

        # \ diagonal
        for i in range(3):
            for j in range(4):
                if self[i, j] != Player.N.value and \
                        self[i, j] == self[i+1, j+1] == self[i+2, j+2] == self[i+3, j+3]:
                    return self[i, j]

        return None

@dataclass
class Game(game_interface.Game):
    current_player: Player = Player.X
    board: Board = Board(Player.N.value * (_XMAX * _YMAX))

    def get_winner(self) -> Optional[Player]:
        winner = self.board._get_winner()
        if winner is not None:
            return Player(winner)
        else:
            return None

    # TODO add support structure to optimize times, updated when applying move
    def get_possible_moves(self) -> List[Move]:
        moves = list()
        for y in range(_YMAX):
            for x in range(_XMAX-1, -1, -1):
                if self.board[x, y] == Player.N.value:
                    moves.append((x, y))
                    break
        return moves

    def apply_move(self, move: Move, deep=True):
        used_game = self._get_used_game(deep)

        self._change_board(move, used_game)
        used_game.current_player = Player.Y if self.current_player == Player.X else Player.X
        return used_game

    def _change_board(self, move, used_game):
        used_game.board[move] = self.current_player

    def _get_used_game(self, deep):
        if deep:
            used_game = Game(self.current_player, Board(self.board.pieces))
        else:
            used_game = self
        return used_game

    def get_hash(self):
        return self.current_player.value + self.board.pieces
