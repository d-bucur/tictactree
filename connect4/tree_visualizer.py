from graphviz import Digraph
from decision_tree import *
import connect4.game


def draw_graph(tree: DecisionTreeNode):
    dot = Digraph(comment='The Round Table')
    _draw_node(dot, tree)
    dot.render('graph-output/decision-tree', format='png', view=True)


def _draw_node(dot, tree: DecisionTreeNode):
    txt = "{}Score: {}\n".format(str(tree.board), tree.score)
    if tree.best_move_pos is None:
        if tree.score < 0:
            txt += "X wins"
            dot.node(str(tree.board), txt, shape='box', color='lightblue2', style='filled')
        elif tree.score > 0:
            txt += "O wins"
            dot.node(str(tree.board), txt, shape='box', color='violetred1', style='filled')
    else:
        txt += "Best: {}".format(str(tree.get_best_move()))
    dot.node(str(tree.board), txt, shape='box')
    for m in tree.moves:
        _draw_node(dot, m.next)
        dot.edge(str(tree.board), str(m.next.board), label=str(m.move))


if __name__ == "__main__":
    my_game = connect4.game.Game()
    connect4.game.board = my_game.board.build_from_str("""
        _______
        __X____
        __X_X__
        __XOO__
        XOOOX__
        XOXOXOX
        """)

    draw_graph(minimax(my_game, 2, True))
