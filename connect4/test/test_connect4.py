import connect4.game
from connect4.game import Player


def test_basic_moves():
    game = connect4.game.Game()
    game.board = game.board.build_from_str("""
        _______
        _______
        _______
        _______
        _______
        XOXOXOX
        """)

    game.board[4, 0] = Player.X
    moves = game.get_possible_moves()
    assert len(moves) == 7
    assert (3, 0) in moves

    game.board = game.board.build_from_str("""
        X______
        O______
        X______
        O______
        X______
        XOXOXO_
        """)
    moves = game.get_possible_moves()
    assert len(moves) == 6

    new_game = game.apply_move((5, 6))
    assert new_game.current_player == Player.Y
    assert game.current_player == Player.X
    moves = new_game.get_possible_moves()
    assert (4, 6) in moves


def test_winner():
    game = connect4.game.Game()
    game.board = game.board.build_from_str("""
        _______
        _______
        _______
        _______
        _______
        XOXOXOX
        """)
    assert game.get_winner() is None

    game.board = game.board.build_from_str("""
        _______
        _______
        _______
        _______
        _XOXXXX
        XOXOXOX
        """)
    assert game.get_winner() == Player.X

    game.board = game.board.build_from_str("""
        _______
        ____O__
        ____O__
        ____O__
        ____O__
        XOXOXOX
        """)
    assert game.get_winner() == Player.Y

    game.board = game.board.build_from_str("""
        _______
        _______
        ___X___
        ___OX__
        _OXOOX_
        XOXOXOX
        """)
    assert game.get_winner() == Player.X

    game.board = game.board.build_from_str("""
        _______
        _______
        ____O__
        ___OX__
        __OXX__
        XOXOXOX
        """)
    assert game.get_winner() == Player.Y