import connect4.game
from decision_tree import minimax


def test_simple_win():
    game = connect4.game.Game()
    game.board = game.board.build_from_str("""
        _______
        __X____
        __X_X__
        __XOO__
        XOOOX__
        XOXOXOX
        """)

    assert minimax(game, 5).get_best_move() == (2, 2)
