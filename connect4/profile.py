import connect4.game
from decision_tree import minimax
import cProfile

if __name__ == "__main__":
    game = connect4.game.Game()
    game.board = game.board.build_from_str("""
            .......
            ..X....
            ..X.X..
            ..XOO..
            XOOOX..
            XOXOXOX
            """)

    pr = cProfile.Profile()
    pr.enable()
    minimax(game, 7)
    pr.disable()
    pr.print_stats(sort='cumtime')
