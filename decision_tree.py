from __future__ import annotations
import operator
import time
from dataclasses import dataclass
from typing import *

from game_interface import Player, Move

total = 0


@dataclass
class DecisionTreeNode:
    score: int
    board: str
    best_move_pos: Optional[int] = None  # position in moves array for best move
    moves: List[DecisionTreeLink] = ()

    def get_best_move(self):
        if self.best_move_pos is None:
            return None
        return self.moves[self.best_move_pos].move


@dataclass
class DecisionTreeLink:
    move: Move
    next: DecisionTreeNode


def minimax(game, max_depth: int=100, debug=False, depth_factor=1) -> DecisionTreeNode:
    global total
    total = 0
    start = time.time()
    m = _minimax(game, 0, dict(), max_depth, debug, depth_factor)
    print("Execution time: {}. Nodes processed: {}".format(time.time() - start, total))
    return m


# TODO consider sub optimal moves in a percentage https://medium.com/ml-everything/tic-tac-toe-and-connect-4-using-mini-max-deb25544f3b7
# TODO profile using class fields instead of params
def _minimax(game, depth, cache: dict, max_depth: int, debug: bool, depth_factor: float) -> DecisionTreeNode:
    MAX_SCORE = 100
    global total

    board_repr = str(game.board) if debug else ""

    if depth >= max_depth:
        return DecisionTreeNode(0, board_repr)

    game_hash = game.get_hash()
    if game_hash in cache:
        return cache[game_hash]

    # Check if node is terminal
    winner = game.get_winner()
    if winner:
        if winner == Player.X:
            return DecisionTreeNode(-MAX_SCORE + depth*depth_factor, board_repr)
        else:
            return DecisionTreeNode(MAX_SCORE - depth*depth_factor, board_repr)
    total += 1

    # Calculate child nodes
    moves = game.get_possible_moves()
    states = [game.apply_move(m) for m in moves]
    if len(states) == 0:
        return DecisionTreeNode(0, board_repr)
    res = [_minimax(s, depth+1, cache, max_depth, debug, depth_factor) for s in states]

    # Find best move in children
    cmp = operator.lt if game.current_player == Player.X else operator.gt
    best_id, best_val = _get_best_move_in_children(cmp, res)

    # Construct node and save to cache
    node = DecisionTreeNode(best_val, board_repr, best_id)
    node.moves = [DecisionTreeLink(moves[i], res[i]) for i in range(len(res))]
    cache[game_hash] = node

    return node


def _get_best_move_in_children(cmp, res):
    best_id = 0
    best_val = res[0].score
    for i in range(1, len(res)):
        if cmp(res[i].score, best_val):
            best_val = res[i].score
            best_id = i
    return best_id, best_val
